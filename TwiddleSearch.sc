TwiddleSearch{
	var  <>synth, <>compVec, <>param, <>spr;//synth param ranges
	var <>errorData, <>bestErrorData;

	*new {arg synthName, vec, parameters=[\freq], ranges=[[100,1200]];
		^super.newCopyArgs(synthName, vec, parameters, ranges);
	}

	sum{
		//assumes input is an array
		arg in;
		var out=0;
		in.do({
			arg val;
			out = out + val;
		});
		^out;
	}

	search{arg def, in=Array.fill(this.param.size, {arg i; rrand(this.spr[i][0], this.spr[i][1])});
		var genVec, error=SynthSearchDif.new, changes= List.newClear, threshold, mfcc, waitTime, bestErr, pTemp=List.new;
		//Routine.run({
		this.errorData=List.new;
		this.bestErrorData=List.new;
		mfcc = SynthSearchAnalysis.new(this.synth);
		//changes, default is 1/4 of maximum safe range/possible range
		for(0, this.param.size() - 1, {arg i;
			var diff;
			diff = spr[i][1] - spr[i][0];
				changes.add(diff * 0.25);
		});
		//calculating threshold as 0.1 of changes
		threshold = (this.sum(changes) * 0.1);
		in.do{
			arg val, i;
			pTemp.add(this.param[i]);
			pTemp.add(val);
		};
		pTemp.postln;
		mfcc.record(def, pTemp.asArray);
		//waitTime.postln;
		pTemp.clear;
		//(waitTime+0.5).wait;
		//mfcc.analyze;

		error.calculate(this.compVec, mfcc.analysis);
		this.errorData.add(error.err);
		bestErr = error.err;

		{this.sum(changes) > threshold}.while( {
			for(0, changes.size - 1, {
				arg i;
				//this.param[((i * 2) + 1)] = (this.param[((i * 2) + 1)] + changes[i]).clip(this.spr[i][0], this.spr[i][1]);
				in[i]= (in[i] + changes[i]).clip(this.spr[i][0], this.spr[i][1]);
				in.do{
					arg val, i;
					pTemp.add(this.param[i]);
					pTemp.add(val);
				};
				mfcc.record(def, pTemp.asArray);
				pTemp.clear;
				//(waitTime+0.5).wait;
				//mfcc.analyze;

				error.calculate(this.compVec, mfcc.analysis);
				this.errorData.add(error.err);

				if(error.err < bestErr, {
					//improvement
					bestErr = error.err;
					this.bestErrorData.add(error.err);
					changes[i] = changes[i] * 1.1;//tweak?
				}, {
					//no improvement
					//this.param[((i * 2) + 1)] = (this.param[((i * 2) + 1)] - (2 * changes[i])).clip(this.spr[i][0], this.spr[i][1]);
					in[i] = (in[i] - (2*changes[i])).clip(this.spr[i][0], this.spr[i][1]);
					in.do{
						arg val, i;
						pTemp.add(this.param[i]);
						pTemp.add(val);
					};
					mfcc.record(def, pTemp);
					pTemp.clear;
					//	(waitTime+0.5).wait;
					//mfcc.analyze;

					error.calculate(this.compVec, mfcc.analysis);
					this.errorData.add(error.err);
					if(error.err < bestErr, {
						//improvement
						bestErr = error.err;
						this.bestErrorData.add(error.err);
						changes[i] = changes[i] * 1.05;
					}, {
						//no improvement
						//this.param[((i * 2) + 1)] = (this.param[((i * 2) + 1)] + changes[i]).clip(this.spr[i][0], this.spr[i][1]);
						//orig value
						in[i] = (in[i] + changes[i]).clip(this.spr[i][0], this.spr[i][1]);
						changes[i] = changes[i] * 0.95;//making smaller steps
					});
				});

			});
		});
		in.do{
			arg val, i;
			pTemp.add(this.param[i]);
			pTemp.add(val);
		};
		this.bestErrorData.asArray.plot;
		this.errorData.asArray.plot;
		^pTemp;
		//});
	}



	*search{arg synthName, analyzedVec, parameters=[\freq, 440], ranges=[[50,1000]], def, in;
		var inpt;
		if(in.isNil, {inpt = Array.fill(parameters.size, {arg i; rrand(ranges[i][0], ranges[i][1])})});
		^this.new(synthName, analyzedVec, parameters, ranges).search(def, if(in.isNil, {inpt},{in}));

	}
}

